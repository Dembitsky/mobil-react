import './App.css';
import  './Fonts.scss';
import  './reset.scss';
import  './Global.scss';
import Header from "./components/Header/Header";


function App() {
  return (
    <Header />
  );
}

export default App;
