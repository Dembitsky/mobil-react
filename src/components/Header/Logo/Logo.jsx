import React from 'react';
import './Logo.scss';
// import logo from '/img/logo.png';

const Logo = () => {
    return (
        <div className="header__logo">
            <img src="./img/logo.png" alt=""/>
            <h1>mobil app</h1>
        </div>
    );
};

export default Logo;