import React from 'react';

const Search = () => {
    return (
        <div className="header__search">
            <input type="text"/>
        </div>
    );
};

export default Search;