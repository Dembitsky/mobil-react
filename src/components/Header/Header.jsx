import React  from 'react';
import './Header.scss';
import Logo from "./Logo/Logo";
import Menu from "./Menu/Menu";
import Search from "./Search/Search";
import Telefon from "./Tel/Telefon";


const Header = () => {
    return (
        <div className="header">
            <div className="container">
                <div className="header__wrapp">
                    <Menu />
                    <Logo />
                    <Search />
                    <Telefon />
                </div>
            </div>
        </div>
    );
};

export default Header;