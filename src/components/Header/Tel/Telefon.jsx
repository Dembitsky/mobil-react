import React from 'react';

const Telefon = () => {
    return (
        <div className="header__tel">
            <a className="header__telefon" href="tel:+380445544455"> 044 55 444 55 </ a>
            <a className="header__link" href="#">Зворотний зв'язок</a>
        </div>
    );
};

export default Telefon;